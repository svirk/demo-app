import React, { useCallback, useEffect, useState } from "react"
import DataEditor, {
	DataEditorProps,
	DataEditorRef,
	GridCell,
	GridCellKind,
	Item,
} from "@glideapps/glide-data-grid"
import { HDict, HGrid, Kind } from "haystack-core"

type GridProps = {
	data?: HGrid
} & Omit<DataEditorProps, "columns" | "getCellContent" | "rows"> &
	React.RefAttributes<DataEditorRef>

const Grid = (props: GridProps): JSX.Element => {
	const [cols, setCols] = useState<{ title: string; id: string }[]>([])
	const [rows, setRows] = useState<HDict[]>([])

	useEffect(() => {
		const result = props.data
		if (!result || result.isEmpty()) {
			setCols([])
		} else {
			const gridRows = result.getRows()
			const cols = result
				.getColumns()
				.map((c) => ({ title: c.displayName, id: c.name }))
			const markerCols = cols.filter((c) =>
				gridRows.some(
					(r) => r.has(c.id) && r.get(c.id)?.getKind() === Kind.Marker
				)
			)
			setCols([
				{
					title: "Markers",
					id: "markers",
				},
				...cols.filter((c) => !markerCols.find((mc) => mc.id === c.id)),
			])
			setRows(gridRows)
		}
	}, [props.data])

	const getCellContent = useCallback(
		(cell: Item): GridCell => {
			const [col, row] = cell
			const rowData = rows[row]
			const cellData = cols[col]
			if (!rowData || !cellData) {
				return {
					kind: GridCellKind.Text,
					allowOverlay: false,
					displayData: "",
					data: "",
				}
			}
			if (cellData.id === "markers") {
				return {
					kind: GridCellKind.Bubble,
					allowOverlay: true,
					data: rowData.keys.filter(
						(k) => rowData.get(k)?.getKind() === Kind.Marker
					),
				}
			}
			const item = rowData.get(cellData.id)
			return {
				kind: GridCellKind.Text,
				allowOverlay: false,
				displayData: item?.toString() ?? "",
				data: item?.toString() ?? "",
			}
		},
		[cols, rows]
	)

	return rows?.length < 1 ? (
		<></>
	) : (
		<DataEditor
			{...props}
			getCellContent={getCellContent}
			columns={cols}
			rows={rows.length}
			smoothScrollX={true}
			smoothScrollY={true}
			onColumnResize={(c, nSize) =>
				setCols(
					cols.map((oc) =>
						oc.id === c.id ? { ...oc, width: nSize } : oc
					)
				)
			}
		/>
	)
}

export default Grid
