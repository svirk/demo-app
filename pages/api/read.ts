import { HGrid } from "haystack-core";
import { Client } from "haystack-nclient";
import { NextApiRequest, NextApiResponse } from "next";

let _client: Client
const curClient = () => {
	if (!_client) {
		const url = process.env.SERVER_URL
		const proj = process.env.PROJ_NAME
		const token = process.env.AUTH_TOKEN

		_client = new Client({
			base: new URL(url as string),
			project: proj,
			authBearer: token
		})
	}

	return _client
}


export default async function handler(req: NextApiRequest, res: NextApiResponse<HGrid>) {
	const filter = req.query?.filter as string

	try {
		const grid = await curClient().ext.eval(filter)

		res.status(200).json(grid)
	} catch (err) {
		console.error(err)
		res.status(500)
	}
}
