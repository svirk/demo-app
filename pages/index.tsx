import { Input } from "antd"
import { HGrid } from "haystack-core"
import type { NextPage } from "next"
import React, { useEffect, useState } from "react"

import "antd/dist/antd.css"
import "../styles/Home.module.css"
// import { ColumnType } from "antd/lib/table"
import dynamic from "next/dynamic"

const Grid = dynamic(() => import("../components/Grid"), { ssr: false })

// const obtainId = (r: HDict) => {
// 	return r.has("id") ? (r.get("id") as HRef).value : Math.random()
// }

declare global {
	interface Window {
		result?: HGrid
	}
}

// const gridColumns = (grid?: HGrid): ColumnType<HDict>[] => {
// 	if (!grid || grid?.isEmpty()) {
// 		return []
// 	}

// 	const rows = grid.getRows()
// 	const cols = grid.getColumns()

// 	const markerCols = cols.filter((c) => {
// 		return rows.some(
// 			(r) => r.has(c.name) && r.get(c.name)?.getKind() === Kind.Marker
// 		)
// 	})

// 	const gridCols = cols
// 		.filter((c) => !markerCols.find((m) => c.name === m.name))
// 		.map((c) => {
// 			return {
// 				title: c.dis,
// 				dataIndex: c.name,
// 				key: c.name,
// 				render(value: unknown, record: HDict, index: number) {
// 					return (
// 						<span>
// 							{record.has(c.name)
// 								? String(record.get(c.name))
// 								: ""}
// 						</span>
// 					)
// 				},
// 			}
// 		})

// 	if (markerCols.length > 0) {
// 		gridCols.unshift({
// 			title: "Markers",
// 			dataIndex: "markers",
// 			key: "markers",
// 			render: (val: unknown, record: HDict, index: number) => {
// 				let letters = 0,
// 					cutoffIndex = 0
// 				// only show markers that are on this record
// 				const markers = markerCols
// 					.filter((col) => record.has(col.name))
// 					.sort((a, b) => a.name.localeCompare(b.name))
// 				return (
// 					<>
// 						{markers
// 							.filter((col, index) => {
// 								const name = col.name
// 								if (letters > 15) {
// 									return
// 								}
// 								if (letters + name.length > 15) {
// 									cutoffIndex = index
// 								}
// 								letters += name.length
// 								return true
// 							})
// 							.map((marker) => (
// 								<Tag key={marker.name} color="blue">
// 									{marker.name}
// 								</Tag>
// 							))}
// 						<Tooltip
// 							title={markers
// 								.slice(cutoffIndex + 1)
// 								.map((marker) => (
// 									<Tag key={marker.name} color="blue">
// 										{marker.name}
// 									</Tag>
// 								))}
// 						>
// 							{markers.length - cutoffIndex - 1 > 0 && (
// 								<Tag color="blue">
// 									+ {markers.length - cutoffIndex - 1}
// 								</Tag>
// 							)}
// 						</Tooltip>
// 					</>
// 				)
// 			},
// 		})
// 	}

// 	return gridCols
// }

const Home: NextPage = () => {
	const [loading, setLoading] = useState(false)
	const [filter, setFilter] = useState("site")
	const [result, setResult] = useState<HGrid>()
	// const [cols, setCols] = useState<{ title: string; id: string }[]>([])
	// const [data, setData] = useState<HDict[]>([])

	useEffect(() => {
		;(async () => {
			setLoading(true)
			try {
				const json = await (
					await fetch(`/api/read?filter=${filter}`)
				).json()
				const grid = HGrid.make(json)
				setResult(grid)
			} catch (err) {
				console.log(err)
			} finally {
				setLoading(false)
			}
		})()
	}, [filter])

	useEffect(() => {
		window.result = result
	}, [result])

	return (
		<>
			<header style={{ padding: 10, borderBottom: "1px solid #d0d0d0" }}>
				<Input.Search
					placeholder="enter filter..."
					enterButton="Go"
					onSearch={(val) => setFilter(val)}
				/>
			</header>
			<div
				className="lastQuery"
				style={{
					backgroundColor: "rgba(24, 144, 255, 0.05)",
					padding: "3px 10px",
					borderBottom: "1px solid #d0d0d0",
				}}
			>
				&raquo; {filter}
			</div>
			<div className="container">
				{/* <Table
					dataSource={result?.getRows()}
					columns={gridColumns(result)}
					className="gridTable"
					pagination={{ pageSize: 50 }}
					loading={loading}
					rowKey={(r) => obtainId(r)}
				/> */}

				<Grid data={result} />
			</div>

			<div
				id="portal"
				style={{ position: "fixed", left: 0, top: 0, zIndex: "9999" }}
			/>
		</>
	)
}

export default Home
